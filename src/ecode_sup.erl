-module(ecode_sup).

-behaviour(supervisor).

%% API
-export([start_link/1]).

%% Supervisor callbacks
-export([init/1]).

%% Helper macro for declaring children of supervisor
-define(CHILD(I, Type), {I, {I, start_link, []}, permanent, 5000, Type, [I]}).

%% ===================================================================
%% API functions
%% ===================================================================

start_link(RootDir) ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, [RootDir]).

%% ===================================================================
%% Supervisor callbacks
%% ===================================================================

init([RootDir]) ->
  {ok, { {one_for_one, 5, 10}, [
    {ecode_server, {ecode_server, start_link, [RootDir ++ "ext/"]}, permanent, brutal_kill, worker, [ecode_server]}
  ]} }.
