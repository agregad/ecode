-module(ecode).

%% API
-export([push/2, pull/2, purge/2]).

pull(Node, Module) ->
  rpc:call(Node, ?MODULE, push, [node(), Module]).

push(Node, Module) ->
  {Module, Binary, _} = code:get_object_code(Module),
  ecode_server:accept(Node, Module, Binary).

purge(Node, Module) ->
  ecode_server:delete(Node, Module).
